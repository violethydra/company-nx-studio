/* ==== IMPORT PARAMS ==== */
import fs from 'fs';
import path from 'path';
import h2mode from 'http2.js';
/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */
const inPub = 'public';
/* ==== ----- ==== */

const browserSync = require('browser-sync').create();

const PATHS = {
	sync: path.join(`${__dirname}\\..\\${inPub}\\404.html`)
};
/* ==== Replace URL or Links ==== */
const __cfg = {
	browserSync: {
		server: {
			baseDir: inPub,
			middleware: (req, res, next) => {
				res.setHeader('Access-Control-Allow-Origin', '*');
				next();
			}
		},
		ghostMode: false,
		notify: false,
		open: false,
		https: true,
		port: 3000,
		httpModule: h2mode,
		// httpModule: 'http2',
		// httpModule: 'spdy',
		// https: {
		// 	key: `${__dirname}\\..\\cerf\\localhost_3000.key`,
		// 	cert: `${__dirname}\\..\\cerf\\localhost_3000.crt`
		// },
		browser: 'chrome.exe'
	}
};
/* ==== ----- ==== */

module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => (

		browserSync.init(__cfg.browserSync, (err, bs) => {
			bs.addMiddleware('*', (req, res) => {
				res.write(fs.readFileSync(PATHS.sync));
				res.end();
			});
		}),
		browserSync.watch(`${inPub}/**/*.*`).on('change', browserSync.reload)

	).on('error',
		_run.notify.onError((err) => (errorConfig(`task: ${nameTask} `, 'ошибка!', err))));
