/* ==== IMPORT PARAMS ==== */
'use strict';
/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */
const
	inDev = 'development',
	inDevApps = `${inDev}/components`,
	inDevTmp = `${inDev}/tmp`;
/* ==== ----- ==== */

const __cfg = {
	svgmin: {
		js2svg: { pretty: true },
		plugins: [
			{ removeDoctype: true },
			{ removeComments: false },
			{ removeXMLProcInst: true },
			{ cleanupNumericValues: { floatPrecision: 2 } },
			{ convertColors: { names2hex: true, rgb2hex: true } },
			{ removeTitle: true },
			{ removeUselessStrokeAndFill: true },
			{ removeDesc: { removeAny: true } },
			{ convertTransform: true }
		]
	},
	cheerio: {
		run: ($, file) => {
			if (!$('svg').attr('viewBox')) {
				const ww = $('svg').attr('width');
				const hh = $('svg').attr('height');
				$('svg').attr('viewBox', `0 0 ${ww} ${hh}`);
			}

			const arr = ['fill', 'stroke', 'style'];
			arr.forEach(i => ($(`[${i}]`).removeAttr(i)));
		},
		parserOptions: { xmlMode: true }
	},
	svgSymbols: {
		slug: { separator: '-' },
		id: 'id-glyphs-%f',
		class: '$%f$',
		templates: ['default-svg', 'default-stylus'],
		svgAttrs: { class: 'inline-svg__init', 'aria-hidden': 'true', version: '1.1' }
	}
};

module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => combiner(
		src(`${inDevApps}/img/__glyphs/**/*.svg`, { allowEmpty: true }),
		_run.cheerio(__cfg.cheerio),
		_run.svgmin(__cfg.svgmin),
		_run.replace('&gt;', '>'),
		_run.svgSymbols(__cfg.svgSymbols),
		dest(`${inDevTmp}`)
	).on('error',
		_run.notify.onError((err) => (errorConfig(`task: ${nameTask} `, 'ошибка!', err))));
